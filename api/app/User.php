<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Laratrust\Traits\LaratrustUserTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @package App
 * @property int id
 * @property string $username
 * @property string $name
 * @property string $email
 */
class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use LaratrustUserTrait, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * A user has a credit account
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function credit()
    {
        return $this->hasOne(Credit::class);
    }

    /**
     * A user has many websites
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function websites()
    {
        return $this->hasMany(Website::class, 'creator_id');
    }

    public function databases()
    {
        return $this->hasMany(Database::class, 'owner_id');
    }

    public function databaseUsers()
    {
        return $this->hasMany(DatabaseUser::class, 'owner_id');
    }

    /**
     * @param $name
     * @return Database|Model
     */
    public function createDatabase($name)
    {
        $name = str_replace('.', '_', $name);
        return $this->databases()->create([
            'name' => "{$this->username}_{$name}"
        ]);
    }

    public function hasCredit($amount = 1)
    {
        return $this->credit->balance >= $amount;
    }

    /**
     * Get User's Home directory
     * @param bool|string $path Path inside home directory
     * @return string
     */
    public function getHomeDirectory($path = false)
    {
        $path = $path ? '/'. trim($path, '/') : '';
        return "/home/{$this->username}" . $path;
    }

    /**
     * Get user's sites directory
     * @return string Path inside sites directory
     */
    public function getSitesDirectory()
    {
        return $this->getHomeDirectory('sites');
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
