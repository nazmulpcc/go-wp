<?php

namespace App\Scripts;

use App\Notifications\ScriptFailNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Foundation\Bus\PendingDispatch;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;

class Script implements ShouldQueue {

    public $tries = 5;
    /**
     * @var Model|null
     */
    protected $model;

    /**
     * The notification class that should handle sending notifications
     * @var bool|string
     */
    protected $notification = false;

    /**
     * Whether we should chain the scripts
     * @var bool
     */
    protected static $chaining = false;

    /**
     * Array of all the chained jobs
     * @var array
     */
    protected static $chain = [];

    /**
     * Whether the Model Owner should receive notification about the execution status
     * @var bool
     */
    protected $subscription = true;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Script constructor.
     * @param Model|null $model
     * @throws \Exception
     */
    public function __construct(Model $model = null)
    {
        $this->model = $model;
        if(! file_exists($this->path())){
            try {
                mkdir($this->path(), 0777, true);
            }catch (\Exception $e){
                throw new \Exception(sprintf("Failed to create script directory %s", $this->path()));
            }
        }
    }

    /**
     * Get the model instance
     * @return Model|null
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Get the user who should be notified about the status of the script execution
     * @return Model|Collection|bool
     */
    public function subscriber()
    {
        return data_get($this->model, 'owner', false);
    }

    /**
     * @return string
     */
    public function script(){
        return "";
    }

    /**
     * Get the script root directory
     * @return string
     */
    public function path(){
        return storage_path('.scripts');
    }

    /**
     * Get the working directory of the script
     * @return string|null
     */
    public function cwd(){
        return null;
    }

    /**
     * Run the script
     * @return bool If the script ran successfully
     */
    public function run()
    {
        $file = $this->saveScript();
        $out = $this->getOutputFilePath();
        $process = Process::fromShellCommandline("bash {$file} > {$out} 2>&1", $this->cwd());
        return !$process->run();
    }

    /**
     * Start processing the job from queue
     */
    public function handle()
    {
        try {
            if(!$this->run()){
                throw new Exception("Error Processing Script");
            }
        }catch (\Exception $e){
            Log::error(sprintf("%s ID: %s Failed", static::class, data_get($this->model, 'id', 'NOT_SAVED')));
            $this->fail($e);
        }
    }

    /**
     * Handle a failed job
     */
    public function failed()
    {
        $subscribers = $this->subscriber();
        if(! $subscribers || ! $this->notification){
            return;
        }
        if($subscribers instanceof Model){
            $this->sendNotificationToSubscriber($subscribers, false);
        }elseif ($subscribers instanceof Collection){
            foreach ($subscribers as $subscriber){
                $this->sendNotificationToSubscriber($subscriber, false);
            }
        }
    }

    /**
     * Send failed job notification to user
     * @param $subscriber
     * @param bool $success
     */
    public function sendNotificationToSubscriber($subscriber, $success = true)
    {
        if(method_exists($subscriber, 'notify')){
            $notification = $this->notification;
            $subscriber->notify(new $notification($this, $success));
        }
    }

    /**
     * Start processing the next jobs in a chain
     */
    public static function startChain()
    {
        static::$chaining = true;
    }

    /**
     * Dispatch the job
     * Returns PendingDispatch instance if the job was dispatched now
     * Returns true if we are waiting for a chain to execute
     * @return true|\Illuminate\Foundation\Bus\PendingDispatch
     */
    public static function dispatch()
    {
        if(! static::$chaining){
            return new PendingDispatch(new static(...func_get_args()));
        }
        static::$chain[] = new static(...func_get_args());
        return true;
    }

    /**
     * Fire away a a chained job
     */
    public static function dispatchChain()
    {
        static::$chaining = false;
        static::dispatch()->chain(static::$chain);
    }

    /**
     * Generate and save the script file
     * @return string Path to where the script was saved
     */
    protected function saveScript(){
        file_put_contents($path = $this->getPath('script.sh'), $this->script());
        return $path;
    }

    /**
     * Get where the output should be written
     * @return string The file path
     */
    protected function getOutputFilePath(){
        return $this->getPath('output');
    }

    /**
     * Generate a path to a file/folder inside script root directory
     * @param string $file Path inside the script root directory
     * @return string Absolute path to the requested file/folder
     */
    protected function getPath($file = ""){
        $base = rtrim($this->path(), '/');
        return $base . '/' . ltrim($file, '/');
    }
}
