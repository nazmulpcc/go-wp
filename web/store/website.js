export const state = () => ({
  websites: [],
  meta: {},
  fetched: false
})

export const mutations = {
  set(state, response) {
    state.websites = response.data
    state.meta = response.meta
    state.fetched = true
  },
  add(state, website) {
    state.websites.push(website)
  },
  unset(state) {
    this.fetched = false
  }
}

export const actions = {
  refresh(context) {
    this.$axios.get('/website').then(({ data }) => {
      context.commit('set', data)
    })
  },
  create(context, data) {
    return this.$axios.post('/website', data).then(({ data }) => {
      console.log('create request data received in store')
      if (data.success) {
        context.commit('add', data.data)
        this.$toast.success('Your website is being deployed.')
      } else {
        this.$toast.failed('Failed to create website')
      }
    })
  }
}
