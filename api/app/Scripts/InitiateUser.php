<?php

namespace App\Scripts;

use App\Role;
use App\User;

class InitiateUser extends Script
{
    /**
     * @var User
     */
    protected $model;
    /**
     * Get the script as string
     * @return array|string
     * @throws \Throwable
     */
    public function script()
    {
        return view('scripts.new-user', [
            'user' => $this->model->username,
            'home' => $this->model->getHomeDirectory(),
            'sites' => $this->model->getSitesDirectory()
        ])->render();
    }

    /**
     * The path where caching will be done
     * @return string
     */
    public function path()
    {
        return storage_path('scripts/new-user/'. $this->model->id);
    }

    public function subscriber()
    {
        return Role::where('name', 'admin')->first()->users;
    }
}
