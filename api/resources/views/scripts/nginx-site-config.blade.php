server {
    listen 443 ssl;
    server_name {{ $domain }};
    root {{ $root }};
    index index.php;

    #log_format custom '$remote_addr -> $remote_user -> [$time_local] -> "$request" -> $status -> $body_bytes_sent -> "$http_referer"  -> "$http_user_agent" -> "$http_x_forwarded_for"';


    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files $uri $uri/ /index.php?$args;

        access_log {{ $root }}/access.log;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires max;
        log_not_found off;
    }
}
