<?php

namespace App\Scripts;

use App\DatabaseUser;

class AttachUserToDatabase extends Script
{
    /**
     * @var DatabaseUser
     */
    protected $model;
    /**
     * Get the script as string
     * @return array|string
     * @throws \Throwable
     */
    public function script()
    {
        return view('scripts.attach-user-to-database', [
            'database' => $this->model->database->name,
            'username' => $this->model->username,
            'permissions' => implode(', ', $this->model->permissions)
        ])->render();
    }

    /**
     * The path where caching will be done
     * @return string
     */
    public function path()
    {
        return storage_path('scripts/attach-user-to-database');
    }
}
