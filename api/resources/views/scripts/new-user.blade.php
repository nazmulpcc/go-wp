if id -u {{ $user }} > /dev/null 2>&1; then
    echo "We already have user: {{ $user }}"
    exit 1
fi
useradd -s /sbin/nologin {{ $user }}
mkdir -p {{ $home }}
mkdir -p {{ $sites }}
chown -R {{ $user }}:{{ $user }} {{ $home }}
chown -R {{ $user }}:{{ $user }} {{ $sites }}
