<?php

namespace App\Jobs;

use App\Scripts\CreateDatabaseUser;
use App\Scripts\CreateUserScript;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateUser implements ShouldQueue
{
    protected $user;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->createUnixUser();
        $this->createDatabaseUser();
    }

    public function createUnixUser()
    {
        $script = new CreateUserScript([
            'user' => $this->user->name
        ]);
        $script->run();
    }

    public function createDatabaseUser()
    {
        $script = new CreateDatabaseUser([
            'user' => $this->user->name
        ]);
        $script->run();
    }
}
