#!/bin/bash

if [[ $DEBUG == "true" ]]; then
    if [ ! -d /etc/letsencrypt/live/$DOMAIN ]; then
        mkdir -p /etc/letsencrypt/live/$DOMAIN
    fi
    openssl genrsa -out "/etc/letsencrypt/live/$DOMAIN/privkey.pem" 2048
    openssl req -new -key "/etc/letsencrypt/live/$DOMAIN/privkey.pem" -out "/etc/letsencrypt/live/$DOMAIN/cert.pem" -subj "/CN=default/O=default/C=UK"
    openssl x509 -req -days 365 -in "/etc/letsencrypt/live/$DOMAIN/cert.pem" -signkey "/etc/letsencrypt/live/$DOMAIN/privkey.pem" -out "/etc/letsencrypt/live/$DOMAIN/fullchain.pem"
    # printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth" > /tmp/sslconfig
    # openssl req -x509 -out /etc/letsencrypt/live/$DOMAIN/cert.pem -keyout /etc/letsencrypt/live/$DOMAIN/privkey.pem \
    #     -newkey rsa:2048 -nodes -sha256 \
    #     -subj '/CN=localhost' -extensions EXT -config /tmp/sslconfig
    echo "Generated local certificates"
else
    echo "dns_cloudflare_email = $CERTBOT_CLOUDFLARE_EMAIL" > ~/cloudflare.ini
    echo "dns_cloudflare_api_key = $CERTBOT_CLOUDFLARE_API"  >> ~/cloudflare.ini

    if [ ! -d /etc/letsencrypt/live/$DOMAIN ]; then
        certbot certonly \
            --dns-cloudflare \
            --dns-cloudflare-credentials ~/cloudflare.ini \
            --agree-tos \
            --email $CERTBOT_EMAIL \
            --non-interactive \
            -d $DOMAIN -d *.$DOMAIN
        if [ ! -d /etc/letsencrypt/live/$DOMAIN ]; then
            cp -r /etc/letsencrypt/live/$DOMAIN* /etc/letsencrypt/live/$DOMAIN
        fi
    fi
    rm ~/cloudflare.ini
fi

mkdir /tmp/ssl
cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem > /tmp/ssl/fullchain.pem
cat /etc/letsencrypt/live/$DOMAIN/privkey.pem > /tmp/ssl/privkey.pem
cat /etc/letsencrypt/live/$DOMAIN/cert.pem > /tmp/ssl/cert.pem

cp /tmp/ssl/* /etc/nginx-ssl