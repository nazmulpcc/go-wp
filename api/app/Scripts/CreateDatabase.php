<?php

namespace App\Scripts;

use App\Database;

class CreateDatabase extends Script
{
    /**
     * @var Database
     */
    protected $model;

    public function script()
    {
        return view('scripts.create-database', [
            'name' => $this->model->name
        ])->render();
    }

    public function path()
    {
        return storage_path('scripts/create-database/'. $this->model->id);
    }
}
