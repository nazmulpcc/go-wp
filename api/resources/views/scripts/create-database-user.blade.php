mysql -h {{ env('DB_HOST') }} -u root -p{{ env('DB_ROOT_PASSWORD') }} -e \
	"CREATE USER '{{ $username }}'@'%' IDENTIFIED BY '{{ $password }}';"
