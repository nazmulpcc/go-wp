echo "Starting init script"
usermod root -a -G sudo
id -u $WORK_USER 1>/dev/null 2>&1
if [ $? -eq 1 ]; then
    adduser $WORK_USER --disabled-password --gecos '' --uid $HOST_USER_ID && \
    usermod $WORK_USER -a -G sudo && \
    groupmod -g $HOST_GROUP_ID $WORK_USER && \
    echo "$WORK_USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers
    chown -R $WORK_USER:$WORK_USER ~
    chown -R $WORK_USER:$WORK_USER ~/.
else
    echo ""
fi

if [ ! -e /etc/nginx-ssl/fullchain.pem ]; then
    bash /root/certbot.sh
    ls /etc/nginx-ssl/
fi

service supervisor start
nginx