export const state = () => ({
  websites: 0,
  databases: 0,
  fetched: false
})

export const mutations = {
  set(state, data) {
    state.websites = data.websites
    state.databases = data.databases
    state.fetched = true
  },
  unset(state) {
    state.fetched = false
  }
}

export const actions = {
  refresh(context) {
    if (context.state.fetched) {
      return
    }
    this.$axios.get('/statistics').then(({ data }) => {
      context.commit('set', data.data)
    })
  }
}
