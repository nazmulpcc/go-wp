<?php

namespace App\Observers;

use App\Scripts\CreateWebsite;
use App\Website;

class WebsiteObserver
{
    /**
     * Handle the website "created" event.
     *
     * @param  \App\Website  $website
     * @return void
     */
    public function created(Website $website)
    {
        CreateWebsite::dispatch($website);
    }

    /**
     * Handle the website "updated" event.
     *
     * @param  \App\Website  $website
     * @return void
     */
    public function updated(Website $website)
    {
        //
    }

    /**
     * Handle the website "deleted" event.
     *
     * @param  \App\Website  $website
     * @return void
     */
    public function deleted(Website $website)
    {
        //
    }

    /**
     * Handle the website "restored" event.
     *
     * @param  \App\Website  $website
     * @return void
     */
    public function restored(Website $website)
    {
        //
    }

    /**
     * Handle the website "force deleted" event.
     *
     * @param  \App\Website  $website
     * @return void
     */
    public function forceDeleted(Website $website)
    {
        //
    }
}
