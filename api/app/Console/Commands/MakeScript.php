<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeScript extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:script {name : Name of the Script} {--view= : The View file name under scripts folder} {--model= : The model required to construct the script}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Script';

    protected $script;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->script = file_get_contents(app_path('Scripts/Script.stub'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $view = $this->option('view') ?: '';
        $model = $this->option('model') ?: 'User';
        if($name == '' || ! $name){
            $this->error('Invalid Name');
        }
        $target = app_path("Scripts/{$name}.php");
        if(file_exists($target)){
            $this->error("$name Already exists");
            return false;
        }
        try {
            $this->replace([
                'SCRIPT_NAME' => $name,
                'VIEW_NAME'   => $view,
                'MODEL_NAME'  => $model
            ]);
            file_put_contents($target, $this->script);
            $this->info("$name Created Successfully");
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }

    protected function replace(array $items){
        $this->script = str_replace(array_keys($items), array_values($items), $this->script);
    }
}
