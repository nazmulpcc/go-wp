<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property string name
 * @property int id
 */
class Database extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(DatabaseUser::class);
    }

    /**
     * @param $username
     * @param bool $password
     * @param bool $permissions
     * @return DatabaseUser|Model
     */
    public function createUser($username, $password = false, $permissions = false)
    {
        $username = $this->owner->username . str_replace('.', '', $username);
        return $this->users()->create([
            'owner_id' => $this->owner_id,
            'username' => $username,
            'password' => $password ?: Str::random(16),
            'permissions' => is_array($permissions) ? $permissions : DatabaseUser::PERMISSIONS
        ]);
    }
}
