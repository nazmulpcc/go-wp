<?php

namespace App\Observers;

use App\Database;
use App\Scripts\CreateDatabase;

class DatabaseObserver
{
    /**
     * Handle the database "created" event.
     *
     * @param  \App\Database  $database
     * @return void
     */
    public function created(Database $database)
    {
        CreateDatabase::dispatch($database);
    }

    /**
     * Handle the database "updated" event.
     *
     * @param  \App\Database  $database
     * @return void
     */
    public function updated(Database $database)
    {
        //
    }

    /**
     * Handle the database "deleted" event.
     *
     * @param  \App\Database  $database
     * @return void
     */
    public function deleted(Database $database)
    {
        //
    }

    /**
     * Handle the database "restored" event.
     *
     * @param  \App\Database  $database
     * @return void
     */
    public function restored(Database $database)
    {
        //
    }

    /**
     * Handle the database "force deleted" event.
     *
     * @param  \App\Database  $database
     * @return void
     */
    public function forceDeleted(Database $database)
    {
        //
    }
}
