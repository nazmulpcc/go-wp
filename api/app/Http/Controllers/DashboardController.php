<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function stats()
    {
        $user = auth()->user();
        return $this->success([
            'websites' => $user->websites()->count(),
            'databases' => $user->databases()->count()
        ]);
    }
}
