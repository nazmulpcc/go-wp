<?php

namespace App\Notifications;

use App\Scripts\CreateWebsite;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateWebsiteNotification extends Notification
{
    use Queueable;
    /**
     * @var Script
     */
    protected $script;
    protected $success;

    /**
     * Create a new notification instance.
     *
     * @param CreateWebsite $script
     * @param $success
     */
    public function __construct(CreateWebsite $script, $success)
    {
        $this->script = $script;
        $this->success = $success;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->getTitle(),
            'message' => $this->getMessage(),
            'item_type' => 'website',
            'item_id' => $this->script->getModel()->id
        ];
    }

    public function getTitle()
    {
        $domain = $this->script->getModel()->domain;
        if($this->success){
            return "Your website {$domain} has been deployed";
        }else{
            return "Sorry! We failed to deploy {$domain}.";
        }
    }

    public function getMessage()
    {
        $domain = $this->script->getModel()->domain;
        if($this->success){
            return "Hooray! Your website is now live at {$domain}";
        }else{
            return "We have received a copy of your deploy request and will start working on it soon. Thank you for your patience.";
        }
    }
}
