<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $guarded = [];

    protected $casts = ['active' => 'bool', 'limits' => 'array'];

    /**
     * Get the a limitation of the subscription
     * @param $key
     * @return mixed
     */
    public function limit($key)
    {
        return data_get($this->limits, $key, null);
    }
}
