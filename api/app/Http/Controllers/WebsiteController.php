<?php

namespace App\Http\Controllers;

use App\Database;
use App\Events\WebsiteCreated;
use App\Http\Requests\WebsiteCreateRequest;
use App\Http\Resources\WebsiteResource;
use App\Jobs\MarkWebsiteAsLive;
use App\Scripts\Script;
use App\User;
use App\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(Request $request)
    {
        $websites = auth()->user()
            ->websites()
            ->latest()
            ->with('database')
            ->paginate($request->input('limit', 10));
        return $this->success(WebsiteResource::collection($websites));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WebsiteCreateRequest $request
     * @return array|\Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(WebsiteCreateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $website = DB::transaction(function () use ($user, $request) {
            Log::info("Starting website job chain");
            Script::startChain();
            $domain = $request->input('domain');
            if($request->input('subdomain')){
                Log::info("Got subdomain $domain");
                $domain = preg_replace('/[^A-Za-z0-9\-]/', '', $domain);
                $domain .= config('app.app_host');
                Log::info("Processed subdomain as $domain");
            }
            $database = $user->createDatabase($domain);
            $database->createUser($domain);
            $data = $request->only(['name', 'admin_user', 'admin_pass']) + ['database_id' => $database->id, 'subscription_id' => 1, 'domain' => $domain];
            $website = $user->websites()->create($data);
            Log::info("Dispatching Jobs");
            Script::dispatchChain();
            return $website;
        });
        return $this->success(new WebsiteResource($website));
    }

    /**
     * Display the specified resource.
     *
     * @param Website $website
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Website $website)
    {
        return $this->success($website);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Website $website
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Website $website)
    {
        return $this->success($website);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Website $website
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Website $website)
    {
        return $this->success();
    }
}
