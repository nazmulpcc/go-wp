<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $user = factory(\App\User::class)->create([
            'name' => 'Nazmul Alam',
            'username' => 'nazmul',
            'email' => 'nazmulpcc@gmail.com'
        ]);
        $user->attachRole('admin');
        factory(\App\User::class)->create([
            'name' => 'Root',
            'username' => 'root',
            'email' => 'admin@admin.com'
        ]);
        factory(\App\User::class)->create([
            'name' => 'Retro',
            'username' => 'retro'
        ]);
    }
}
