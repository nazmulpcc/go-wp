<?php

namespace App\Observers;

use App\DatabaseUser;
use App\Scripts\AttachUserToDatabase;
use App\Scripts\CreateDatabaseUser;

class DatabaseUserObserver
{
    /**
     * Handle the database user "created" event.
     *
     * @param  \App\DatabaseUser  $databaseUser
     * @return void
     */
    public function created(DatabaseUser $user)
    {
        CreateDatabaseUser::dispatch($user);
        AttachUserToDatabase::dispatch($user);
    }

    /**
     * Handle the database user "updated" event.
     *
     * @param  \App\DatabaseUser  $databaseUser
     * @return void
     */
    public function updated(DatabaseUser $databaseUser)
    {
        //
    }

    /**
     * Handle the database user "deleted" event.
     *
     * @param  \App\DatabaseUser  $databaseUser
     * @return void
     */
    public function deleted(DatabaseUser $databaseUser)
    {
        //
    }
}
