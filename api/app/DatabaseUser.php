<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Database database
 * @property string username
 * @property string password
 * @property array permissions
 */
class DatabaseUser extends Model
{
    protected $guarded = [];

    public const PERMISSIONS = ['SELECT', 'INSERT', 'UPDATE', 'DELETE', 'CREATE', 'INDEX', 'ALTER', 'LOCK TABLES'];

    protected $casts = [
        'permissions' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function database()
    {
        return $this->belongsTo(Database::class);
    }
}
