<?php

namespace App\Jobs;

use App\Scripts\CreateDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateWebsite implements ShouldQueue
{
    protected $website;

    public $timeout = 0;
    
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param $website
     */
    public function __construct($website)
    {
        $this->website = $website;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->createDatabase();
        $this->createWebsite();
    }

    protected function createDatabase(){
        $script = new CreateDatabase([
            'user' => $this->website->user->name,
            'db_name' => $this->website->db_name
        ]);
        $script->run();
    }

    protected function createWebsite(){
        $script = new \App\Scripts\CreateWebsite([
            'user' => $this->website->user->name,
            'domain' => $this->website->domain,
            'db_name' => $this->website->db_name,
            'db_user' => $this->website->user->name,
            'db_pass' => 'secret',
            'root' => "/home/". $this->website->user->name . "/sites/". $this->website->domain
        ]);
        $script->run();
    }
}
