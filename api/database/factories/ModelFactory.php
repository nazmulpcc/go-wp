<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Website;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('secret'), // password
    ];
});

$factory->define(\App\Database::class, function (Faker $faker){
    return [
        'owner_id' => function(){
            return factory(User::class)->create()->id;
        },
        'name' => "{$faker->word}_{$faker->word}"
    ];
});

$factory->define(\App\DatabaseUser::class, function (Faker $faker){
    $user = factory(User::class)->create();
    return [
        'owner_id' => $user->id,
        'database_id' => function() use ($user) {
            return factory(\App\Database::class)->create(['owner_id' => $user->id])->id;
        },
        'username' => "{$user->username}_{$faker->word}",
        'password' => Str::random(16),
        'permissions' => \App\DatabaseUser::PERMISSIONS
    ];
});

$factory->define(Website::class, function (Faker $faker){
    $name = $faker->domainWord;
    $tld = $faker->tld;
    $user = factory(User::class)->create();
    return [
        'name' => $name,
        'domain' => "{$name}.{$tld}",
        'creator_id' => $user->id,
        'database_id' => function(){
            return factory(\App\Database::class)->create()->id;
        },
        'subscription_id' => 1,
        'admin_user' => encrypt($user),
        'admin_pass' => encrypt('secret')
    ];
});
