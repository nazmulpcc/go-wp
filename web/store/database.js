export const state = () => ({
  databases: [],
  meta: {},
  fetched: false
})

export const mutations = {
  set(state, response) {
    state.databases = response.data
    state.meta = response.meta
    state.fetched = true
  },
  unset(state) {
    state.fetched = false
  }
}

export const actions = {
  refresh(context) {
    this.$axios.get('/database').then(({ data }) => {
      context.commit('set', data)
    })
  }
}
