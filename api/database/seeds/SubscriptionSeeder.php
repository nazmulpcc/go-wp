<?php

use App\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscription::create([
            'name' => 'Free',
            'shortname' => 'free',
            'limits' => ['storage' => 100, 'network' => 1024]
        ]);
        Subscription::create([
            'name' => 'Basic',
            'shortname' => 'basic',
            'limits' => ['storage' => 1024, 'network' => 10 * 1024]
        ]);
        Subscription::create([
            'name' => 'Advanced',
            'shortname' => 'advanced',
            'limits' => ['storage' => 10 * 1024, 'network' => 100 * 1024]
        ]);
    }
}
