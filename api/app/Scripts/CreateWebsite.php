<?php

namespace App\Scripts;

use App\Notifications\CreateWebsiteNotification;
use App\Website;

class CreateWebsite extends Script
{
    /**
     * @var Website
     */
    protected $model;

    protected $notification = CreateWebsiteNotification::class;

    public function handle()
    {
        parent::handle();
        $this->model->update(['status' => Website::LIVE]);
    }

    public function script()
    {
        $dbUser = $this->model->database->users()->first();
        return view('scripts.create-website', [
            'config' => $this->createVhostConfig(),
            'user' => $this->model->owner->username,
            'domain' => $this->model->domain,
            'db_name' => $this->model->database->name,
            'db_user' => $dbUser->username,
            'db_pass' => $dbUser->password,
            'admin_email' => $this->model->owner->email,
            'admin_user' => $this->model->admin_user,
            'admin_pass' => $this->model->admin_pass,
            'root' => "/home/{$this->model->owner->username}/sites/{$this->model->domain}"
        ])->render();
    }

    public function path()
    {
        return storage_path('scripts/create-website/'. $this->model->id);
    }

    public function createVhostConfig(){
        $file = $this->getPath('vhost');
        file_put_contents($file, view('scripts.nginx-site-config', [
            'root' => "/home/{$this->model->owner->username}/sites/{$this->model->domain}",
            'domain' => $this->model->domain
        ])->render());
        return $file;
    }
}
