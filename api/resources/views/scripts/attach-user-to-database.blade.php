mysql -h {{ env('DB_HOST') }} -u root -p{{ env('DB_ROOT_PASSWORD') }} -e \
	"GRANT {{ $permissions }} ON {{ $database }}.* TO '{{ $username }}'@'%'; FLUSH PRIVILEGES;"
