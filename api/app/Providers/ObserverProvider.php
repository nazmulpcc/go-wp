<?php

namespace App\Providers;

use App\Database;
use App\DatabaseUser;
use App\Observers\DatabaseObserver;
use App\Observers\DatabaseUserObserver;
use App\Observers\UserObserver;
use App\Observers\WebsiteObserver;
use App\User;
use App\Website;
use Illuminate\Support\ServiceProvider;

class ObserverProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Database::observe(DatabaseObserver::class);
        DatabaseUser::observe(DatabaseUserObserver::class);
        Website::observe(WebsiteObserver::class);
    }
}
