<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WebsiteCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:200|min:5',
            'domain' => 'required|unique:websites,domain',
            'admin_user' => 'required|max:200|min:3|alphanum',
            'admin_pass' => 'required|max:200|min:6'
        ];
    }
}
