<?php

use Illuminate\Support\Facades\Route;

Route::prefix('auth')->middleware('api')->group(function (){
    Illuminate\Support\Facades\Auth::routes(['verify' => true]);
    Route::post('refresh', 'ApiAuthController@refresh');
    Route::get('user', 'ApiAuthController@user');
});

Route::middleware('auth:api')->group(function(){
    Route::get('/statistics', 'DashboardController@stats');
});

Route::middleware('auth:api')
    ->resource('website', 'WebsiteController')
    ->only(['index', 'show', 'store']);

Route::middleware('auth:api')
    ->resource('database', 'DatabaseController')
    ->only(['index', 'show']);
