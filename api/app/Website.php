<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static Website create(array $data)
 * @property User owner
 * @property Database database
 * @property string domain
 * @property string admin_user
 * @property string admin_pass
 */
class Website extends Model
{
    protected $guarded = [];

    protected $hidden = ['db_password'];

    const DEPLOYING = 'DEPLOYING';
    const LIVE = 'LIVE';
    const QUEUED = 'QUEUED';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner(){
        return $this->belongsTo(User::class, 'creator_id');
    }

    /**
     * An website must have a database
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function database()
    {
        return $this->belongsTo(Database::class);
    }
}
