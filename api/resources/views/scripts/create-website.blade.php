mkdir -p {{ $root }}
cd {{ $root }}

wpcl core download --allow-root
wpcl config create --dbname={{ $db_name }} --dbuser={{ $db_user }} --dbpass={{ $db_pass }} --dbhost=mysql --allow-root
wpcl core install --url=https://{{ $domain }} --title="Hello World" --admin_user={{ $admin_user }} --admin_password={{ $admin_pass }} --admin_email={{ $admin_email }} --skip-email --allow-root

chown -R {{ $user }}:{{ $user }} {{ $root }}
find {{ $root }}/. -type d -exec chmod 755 {} \;
find {{ $root }}/. -type f -exec chmod 644 {} \;
chown -R www-data:www-data {{ $root }}/wp-content
chmod -R 777 {{ $root }}/wp-content
echo "define('FS_METHOD','direct');" >> {{ $root }}/wp-config.php

@include('scripts.create-vhost')

echo "Site created. Reloading Server with new config."

nginx -s reload
