<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebsiteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only(['name', 'domain', 'admin_user']) + [
            'status' => ucfirst(strtolower($this->status)),
            'database' => $this->database->name,
            'created' => $this->created_at->format('d F, Y')
        ];
    }
}
