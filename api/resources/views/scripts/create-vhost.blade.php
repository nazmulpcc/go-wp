cp {{ $config }} /var/sites/{{ $domain }}

if [ -e /etc/nginx/sites-enabled/{{ $domain }} ]; then
    echo "Website config already exists: /etc/nginx/sites-enabled/{{ $domain }}"
    echo "Replacing Config"
    rm /etc/nginx/sites-enabled/{{ $domain }}
fi

ln -s /var/sites/{{ $domain }} /etc/nginx/sites-enabled/
