<?php


namespace App\Scripts;


use App\DatabaseUser;

class CreateDatabaseUser extends Script
{
    /**
     * @var DatabaseUser
     */
    protected $model;

    public function path()
    {
        return storage_path('scripts/create-database-user/'. $this->model->id);
    }

    public function script()
    {
        return view('scripts.create-database-user', [
            'username' => $this->model->username,
            'password' => $this->model->password
        ])->render();
    }
}
