<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function failed($data = null, $message = '', $code = 400)
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'data'    => $data
        ], $code);
    }

    public function success($data = null, $message = '', $code = 200)
    {
        $response = [
            'success' => true,
            'message' => $message
        ];
        if($data instanceof LengthAwarePaginator){
            $response += $data->toArray();
        }elseif ($data instanceof JsonResource){
            $response = $data->additional($response);
        }else{
            $response['data'] = $data;
        }
        return $response;
    }
}
